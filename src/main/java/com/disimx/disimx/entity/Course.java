package com.disimx.disimx.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by lorenzo on 5/28/17.
 */
@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "nameIt")
    private String nameIt;
    @Column(name = "code")
    private String code;
    @Column(name = "language")
    private String language;
    @Column(name = "languageIt")
    private String languageIt;
    @Lob
    @Column(name = "description")
    private String description;
    @Lob
    @Column(name = "description_it")
    private String descriptionIt;
    @Lob
    @Column(name = "dub_knowledge")
    private String dubKnowledge;
    @Lob
    @Column(name = "dub_knowledge_it")
    private String dubKnowledgeIt;
    @Lob
    @Column(name = "dub_application")
    private String dubApplication;
    @Lob
    @Column(name = "dub_application_it")
    private String dubApplicationIt;
    @Lob
    @Column(name = "dub_evaluation")
    private String dubEvaluation;
    @Lob
    @Column(name = "dub_evaluation_it")
    private String dubEvaluationIt;
    @Lob
    @Column(name = "dub_communication")
    private String dubCommunication;
    @Lob
    @Column(name = "dub_communication_it")
    private String dubCommunicationIt;
    @Lob
    @Column(name = "dub_skills")
    private String dubSkills;
    @Lob
    @Column(name = "dub_skills_it")
    private String dubSkillsIt;
    @Column(name = "semester")
    private Integer semester;
    @Column(name = "year")
    private Integer year;
    @Lob
    @Column(name = "note")
    private String note;
    @Lob
    @Column(name = "note_it")
    private String noteIt;
    @Column(name = "mod_esame")
    private String modEsame;
    @Column(name = "mod_esame_it")
    private String modEsameIt;
    @Column(name = "mod_insegnamento")
    private String modInsegnamento;
    @Column(name = "mod_insegnamento_it")
    private String modInsegnamentoIt;
    @Lob
    @Column(name = "programme")
    private String programme;
    @Lob
    @Column(name = "programme_it")
    private String programmeIt;
    @Column(name = "resources")
    private String resources;
    @Column(name = "resources_it")
    private String resourcesIt;
    @OneToOne
    @JoinColumn(name = "scientific_area", insertable = false, updatable = false)
    private ScientificArea scientificArea;
    @Column(name = "scientific_area_id")
    private Integer scientificAreaId;
    @Lob
    @Column(name = "books")
    private String books;
    @ManyToMany
    @JoinTable(name = "prof_course", joinColumns = @JoinColumn(name = "course_id"), inverseJoinColumns = @JoinColumn(name = "prof_id"))
    private List<User> profs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameIt() {
        return nameIt;
    }

    public void setNameIt(String nameIt) {
        this.nameIt = nameIt;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageIt() {
        return languageIt;
    }

    public void setLanguageIt(String languageIt) {
        this.languageIt = languageIt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionIt() {
        return descriptionIt;
    }

    public void setDescriptionIt(String descriptionIt) {
        this.descriptionIt = descriptionIt;
    }

    public String getDubKnowledge() {
        return dubKnowledge;
    }

    public void setDubKnowledge(String dubKnowledge) {
        this.dubKnowledge = dubKnowledge;
    }

    public String getDubKnowledgeIt() {
        return dubKnowledgeIt;
    }

    public void setDubKnowledgeIt(String dubKnowledgeIt) {
        this.dubKnowledgeIt = dubKnowledgeIt;
    }

    public String getDubApplication() {
        return dubApplication;
    }

    public void setDubApplication(String dubApplication) {
        this.dubApplication = dubApplication;
    }

    public String getDubApplicationIt() {
        return dubApplicationIt;
    }

    public void setDubApplicationIt(String dubApplicationIt) {
        this.dubApplicationIt = dubApplicationIt;
    }

    public String getDubEvaluation() {
        return dubEvaluation;
    }

    public void setDubEvaluation(String dubEvaluation) {
        this.dubEvaluation = dubEvaluation;
    }

    public String getDubEvaluationIt() {
        return dubEvaluationIt;
    }

    public void setDubEvaluationIt(String dubEvaluationIt) {
        this.dubEvaluationIt = dubEvaluationIt;
    }

    public String getDubCommunication() {
        return dubCommunication;
    }

    public void setDubCommunication(String dubCommunication) {
        this.dubCommunication = dubCommunication;
    }

    public String getDubSkills() {
        return dubSkills;
    }

    public void setDubSkills(String dubSkills) {
        this.dubSkills = dubSkills;
    }

    public String getDubSkillsIt() {
        return dubSkillsIt;
    }

    public void setDubSkillsIt(String dubSkillsIt) {
        this.dubSkillsIt = dubSkillsIt;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public ScientificArea getScientificArea() {
        return scientificArea;
    }

    public void setScientificArea(ScientificArea scientificArea) {
        this.scientificArea = scientificArea;
    }

    public Integer getScientificAreaId() {
        return scientificAreaId;
    }

    public void setScientificAreaId(Integer scientificAreaId) {
        this.scientificAreaId = scientificAreaId;
    }

    public List<User> getProfs() {
        return profs;
    }

    public void setProfs(List<User> profs) {
        this.profs = profs;
    }

    public String getDubCommunicationIt() {
        return dubCommunicationIt;
    }

    public void setDubCommunicationIt(String dubCommunicationIt) {
        this.dubCommunicationIt = dubCommunicationIt;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteIt() {
        return noteIt;
    }

    public void setNoteIt(String noteIt) {
        this.noteIt = noteIt;
    }

    public String getModEsame() {
        return modEsame;
    }

    public void setModEsame(String modEsame) {
        this.modEsame = modEsame;
    }

    public String getModEsameIt() {
        return modEsameIt;
    }

    public void setModEsameIt(String modEsameIt) {
        this.modEsameIt = modEsameIt;
    }

    public String getModInsegnamento() {
        return modInsegnamento;
    }

    public void setModInsegnamento(String modInsegnamento) {
        this.modInsegnamento = modInsegnamento;
    }

    public String getModInsegnamentoIt() {
        return modInsegnamentoIt;
    }

    public void setModInsegnamentoIt(String modInsegnamentoIt) {
        this.modInsegnamentoIt = modInsegnamentoIt;
    }

    public String getProgramme() {
        return programme;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    public String getProgrammeIt() {
        return programmeIt;
    }

    public void setProgrammeIt(String programmeIt) {
        this.programmeIt = programmeIt;
    }

    public String getResources() {
        return resources;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }

    public String getResourcesIt() {
        return resourcesIt;
    }

    public void setResourcesIt(String resourcesIt) {
        this.resourcesIt = resourcesIt;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }
}
