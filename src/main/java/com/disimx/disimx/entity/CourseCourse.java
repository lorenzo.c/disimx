package com.disimx.disimx.entity;

import javax.persistence.*;

/**
 * Created by lorenzo on 5/28/17.
 */
@Entity
@Table(name = "course_course")
public class CourseCourse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "_id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "course_id", insertable = false, updatable = false)
    private Course course;
    @Column(name = "course_id")
    private Long courseId;
    @ManyToOne
    @JoinColumn(name = "child_course_id", insertable = false, updatable = false)
    private Course childCourse;
    @Column(name = "child_course_id")
    private Long childCourseId;
    @Column(name = "mutuato")
    private Integer mutuato;
    @Column(name = "propedeutico")
    private Integer propedeutico;
    @Column(name = "module")
    private Integer module;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getChildCourse() {
        return childCourse;
    }

    public void setChildCourse(Course childCourse) {
        this.childCourse = childCourse;
    }

    public Long getChildCourseId() {
        return childCourseId;
    }

    public void setChildCourseId(Long childCourseId) {
        this.childCourseId = childCourseId;
    }

    public Integer getMutuato() {
        return mutuato;
    }

    public void setMutuato(Integer mutuato) {
        this.mutuato = mutuato;
    }

    public Integer getPropedeutico() {
        return propedeutico;
    }

    public void setPropedeutico(Integer propedeutico) {
        this.propedeutico = propedeutico;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }
}
