package com.disimx.disimx.repository;

import com.disimx.disimx.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRole(String admin);

    Role findById(Integer id);
}
