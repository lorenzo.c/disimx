package com.disimx.disimx.repository;

import com.disimx.disimx.entity.Role;
import com.disimx.disimx.entity.ScientificArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientificAreaRepository extends JpaRepository<ScientificArea, Long> {


}
