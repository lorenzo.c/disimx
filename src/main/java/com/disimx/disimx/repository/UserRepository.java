package com.disimx.disimx.repository;

import com.disimx.disimx.entity.Role;
import com.disimx.disimx.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    List<User> findByIdIn(List<Long> userIds);

    List<User> findByRoles(Role role);

    Boolean existsByEmail(String email);
}
