package com.disimx.disimx.repository;

import com.disimx.disimx.entity.Course;
import com.disimx.disimx.entity.ScientificArea;
import com.disimx.disimx.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    Course findByProfsAndId(User prof, long course_id);

    List<Course> findByProfs(User prof);

    List<Course> findByYear(int year);

    Optional<Course> findByCodeAndYear(String code, int year);

    List<Course> findByCode(String code);
}
