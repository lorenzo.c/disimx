package com.disimx.disimx.repository;

import com.disimx.disimx.entity.Course;
import com.disimx.disimx.entity.ScientificArea;
import com.disimx.disimx.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoursePageRepository extends PagingAndSortingRepository<Course, Long> {
    Page<Course> findByNameContainingOrNameItContainingOrDescriptionContainingOrDescriptionItContaining(Pageable pageable, String name, String nameIt, String description, String descriptionIt);

    Page<Course> findByProfs(Pageable pageable, User prof);
}
