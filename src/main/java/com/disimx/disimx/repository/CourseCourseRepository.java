package com.disimx.disimx.repository;

import com.disimx.disimx.entity.Course;
import com.disimx.disimx.entity.CourseCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CourseCourseRepository extends JpaRepository<CourseCourse, Long> {
    CourseCourse findFirstByCourse(Course course);

    List<CourseCourse> findByCourseAndModule(Course course, Integer integer);

    List<CourseCourse> findByCourseAndMutuato(Course course, Integer integer);

    List<CourseCourse> findByCourseAndPropedeutico(Course course, Integer integer);

    List<CourseCourse> findByCourseAndChildCourseIdAndModule(Course course, Long childCourseId, Integer integer);

    List<CourseCourse> findByCourseAndChildCourseIdAndMutuato(Course course, Long childCourseId, Integer integer);

    List<CourseCourse> findByCourseAndChildCourseIdAndPropedeutico(Course course, Long childCourseId, Integer integer);

    @Modifying
    @Transactional
    @Query("delete from CourseCourse u where u.id = ?1")
    void deleteByCourseId(Long courseId);
}
