package com.disimx.disimx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DisimxApplication {

	public static void main(String[] args) {
		SpringApplication.run(DisimxApplication.class, args);
	}
}
