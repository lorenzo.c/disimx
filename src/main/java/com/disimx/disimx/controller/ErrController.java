package com.disimx.disimx.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ErrController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public void error(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendRedirect("/");
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
