package com.disimx.disimx.controller;

import com.disimx.disimx.entity.Course;
import com.disimx.disimx.entity.CourseCourse;
import com.disimx.disimx.entity.Pager;
import com.disimx.disimx.entity.User;
import com.disimx.disimx.model.CourseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class CourseController {
    private static final int BUTTONS_TO_SHOW = 5;
    private static final int INITIAL_PAGE = 0;
    private static final int INITIAL_PAGE_SIZE = 6;
    private static final int[] PAGE_SIZES = {6, 12, 18};
    @Autowired
    private CourseModel courseModel;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/course/modify", method = RequestMethod.GET)
    public ModelAndView getCreateOrModifyPage(@RequestParam(value = "courseId", required = false) final Long courseId, @RequestParam(value = "year", required = false) final Integer year, Principal principal) {
        String email = principal.getName();
        ModelAndView mav = new ModelAndView("course/admin_modify");
        Course course = new Course();
        if (courseId != null) {
            Optional<Course> optional = courseModel.getCourse(courseId);
            if (optional.isPresent())
                course = optional.get();
            logger.info("User " + email + " requesting course: " + courseId);
            if (year != null && course.getYear() != year)
                course = courseModel.getCourse(course.getCode(), year).get();
        }
        if (course.getScientificArea() == null && course.getScientificAreaId() != null)
            course.setScientificArea(courseModel.getScientificArea(course.getScientificAreaId()));
        mav.addObject("course", course);
        mav.addObject("courses", courseModel.getCourses());
        mav.addObject("moduloRelationChildren", courseModel.getModuloRelationChildren(course));
        mav.addObject("moduloMutuatoChildren", courseModel.getMutuatoRelation(course));
        mav.addObject("moduloPropedeuticoChildren", courseModel.getPropedeuticoRelation(course));
        mav.addObject("scientificAreas", courseModel.getScientificAreas());
        mav.addObject("profs", courseModel.getProfs());
        return mav;
    }

    @RequestMapping(value = "/prof/course/modify", method = RequestMethod.GET)
    public ModelAndView profCreateOrModify(@RequestParam(value = "courseId") final Long courseId, @RequestParam(value = "year", required = false) final Integer year, Principal principal) {
        String email = principal.getName();
        ModelAndView mav = new ModelAndView("course/prof_modify");
        Course course = new Course();
        Optional<Course> optional = courseModel.getCourse(courseId);
        if (optional.isPresent())
            course = optional.get();
        logger.info("User " + email + " requesting course: " + courseId);
        if (year != null && course.getYear() != year)
            course = courseModel.getCourse(course.getCode(), year).get();
        if (course.getScientificArea() == null && course.getScientificAreaId() != null)
            course.setScientificArea(courseModel.getScientificArea(course.getScientificAreaId()));
        mav.addObject("course", course);
        // mav.addObject("courses",courseModel.getCourses());
        mav.addObject("scientificAreas", courseModel.getScientificAreas());
        mav.addObject("profs", courseModel.getProfs());
        return mav;
    }

    @RequestMapping(value = "/course/modify", method = RequestMethod.POST)
    public void adminCreateOrModify(@Valid Course course, @RequestParam(value = "relation_course_modulo_id[]", required = false) Integer[] relationModuloId,
                                            @RequestParam(value = "relation_course_mutuato_id[]", required = false) Integer[] relationMutuatoId,
                                            @RequestParam(value = "relation_course_propedeutico_id[]", required = false) Integer[] relationPropedeuticoId,
                                            Principal principal, HttpServletResponse httpServletResponse) throws IOException {
        String email = principal.getName();
        logger.info("User " + email + " modifying course: " + course.getName());
        if (course.getScientificArea() == null && course.getScientificAreaId() != null)
            course.setScientificArea(courseModel.getScientificArea(course.getScientificAreaId()));
        if (course.getProfs() == null)
            course.setProfs(courseModel.getProfs(course));
        courseModel.save(course);
        httpServletResponse.sendRedirect("/course/show?courseId=" + course.getId());
        courseModel.saveRelations(relationModuloId, relationMutuatoId, relationPropedeuticoId, course);
    }

    @RequestMapping(value = "/course/show", method = RequestMethod.GET)
    public ModelAndView showCourse(@RequestParam(value = "courseId") final Long courseId, @RequestParam(value = "year", required = false) final Integer year) throws IOException {
        ModelAndView mav = new ModelAndView("course/show");
        Course course = new Course();
        Optional<Course> optional = courseModel.getCourse(courseId);
        if (optional.isPresent()) {
            course = optional.get();
        }
        if (year != null && course.getYear() != year)
            course = courseModel.getCourse(course.getCode(), year).get();
        if (course.getScientificArea() == null && course.getScientificAreaId() != null)
            course.setScientificArea(courseModel.getScientificArea(course.getScientificAreaId()));
        mav.addObject("course", course);
        mav.addObject("moduloRelationChildren", courseModel.getModuloRelationChildrenCourse(course));
        mav.addObject("mutuatoRelationChildren", courseModel.getMutuatoRelationCourse(course));
        mav.addObject("propedeuticoRelationChildren", courseModel.getPropedeuticoRelationCourse(course));
        mav.addObject("years", courseModel.getYears(course));
        return mav;
    }

    @RequestMapping("/")
    public ModelAndView getCourses(@RequestParam("pageSize") Optional<Integer> pageSize,
                                   @RequestParam("page") Optional<Integer> page
    ) {
        ModelAndView mav = new ModelAndView("course/index");

        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Course> courses = courseModel.getCoursesPage(PageRequest.of(evalPage, evalPageSize));
        Pager pager = new Pager(courses.getTotalPages(), courses.getNumber(), BUTTONS_TO_SHOW);

        mav.addObject("courses", courses);
        mav.addObject("selectedPageSize", evalPageSize);
        mav.addObject("pageSizes", PAGE_SIZES);
        mav.addObject("pager", pager);


        return mav;
    }

    @RequestMapping("/admin/courses")
    public ModelAndView adminGetCourses(Principal principal,
                                        HttpServletResponse httpServletResponse,
                                        @RequestParam("pageSize") Optional<Integer> pageSize,
                                        @RequestParam("page") Optional<Integer> page) throws IOException {
        String email = principal.getName();
        User currentUser = courseModel.getUser(email);
        Page<Course> courses = null;
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;
        if (currentUser.getRoles().contains(courseModel.getAdmin())) {
            courses = courseModel.getCoursesPage(PageRequest.of(evalPage, evalPageSize));
            logger.info("User " + email + " requestying courses: " + courses.getTotalElements());
        } else {
            httpServletResponse.sendRedirect("/");
            logger.info("User " + email + " doesn't own permissions to view this page");
        }
        Pager pager = new Pager(courses.getTotalPages(), courses.getNumber(), BUTTONS_TO_SHOW);
        ModelAndView mav = new ModelAndView("course/backoffice_admin_index");
        mav.addObject("courses", courses);
        mav.addObject("selectedPageSize", evalPageSize);
        mav.addObject("pageSizes", PAGE_SIZES);
        mav.addObject("pager", pager);
        return mav;
    }

    @RequestMapping("/prof/courses")
    public ModelAndView profGetCourses(Principal principal,
                                       HttpServletResponse httpServletResponse) throws IOException {
        String email = principal.getName();
        User currentUser = courseModel.getUser(email);
        List<Course> courses = null;
        if (currentUser.getRoles().contains(courseModel.getProf())) {
            courses = courseModel.getCourses(currentUser);
            logger.info("User " + email + " requestying courses: " + courses.size());
        } else {
            httpServletResponse.sendRedirect("/");
            logger.info("User " + email + " doesn't own permissions to view this page");
        }
        ModelAndView mav = new ModelAndView("course/backoffice_index");
        mav.addObject("courses", courses);
        return mav;
    }

    @RequestMapping("/admin/courses/search")
    public ModelAndView searchCoursesAdmin(@RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page,
                                      @RequestParam(value = "keyword") final String keyword) throws IOException {
        ModelAndView mav = new ModelAndView("course/backoffice_admin_index");

        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Course> courses = courseModel.getCoursesPageSearch(PageRequest.of(evalPage, evalPageSize), keyword);
        Pager pager = new Pager(courses.getTotalPages(), courses.getNumber(), BUTTONS_TO_SHOW);

        mav.addObject("courses", courses);
        mav.addObject("selectedPageSize", evalPageSize);
        mav.addObject("pageSizes", PAGE_SIZES);
        mav.addObject("pager", pager);


        return mav;
    }

    @RequestMapping("/courses/search")
    public ModelAndView searchCourses(@RequestParam("pageSize") Optional<Integer> pageSize, @RequestParam("page") Optional<Integer> page,
                                      @RequestParam(value = "keyword") final String keyword) throws IOException {
        ModelAndView mav = new ModelAndView("course/index");

        // Evaluate page size. If requested parameter is null, return initial
        // page size
        int evalPageSize = pageSize.orElse(INITIAL_PAGE_SIZE);
        // Evaluate page. If requested parameter is null or less than 0 (to
        // prevent exception), return initial size. Otherwise, return value of
        // param. decreased by 1.
        int evalPage = (page.orElse(0) < 1) ? INITIAL_PAGE : page.get() - 1;

        Page<Course> courses = courseModel.getCoursesPageSearch(PageRequest.of(evalPage, evalPageSize), keyword);
        Pager pager = new Pager(courses.getTotalPages(), courses.getNumber(), BUTTONS_TO_SHOW);

        mav.addObject("courses", courses);
        mav.addObject("selectedPageSize", evalPageSize);
        mav.addObject("pageSizes", PAGE_SIZES);
        mav.addObject("pager", pager);


        return mav;
    }
}
