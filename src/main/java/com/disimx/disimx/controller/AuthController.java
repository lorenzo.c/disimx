package com.disimx.disimx.controller;

import com.disimx.disimx.entity.Role;
import com.disimx.disimx.entity.User;
import com.disimx.disimx.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
public class AuthController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserModel userModel;

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public ModelAndView getLoginPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /**
     * pagina signup
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView getRegistrationPage() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    /**
     * registra un nuove utente
     * @param user [User] id del gioco
     * @param bindingResult [BindingResult] punti ottenuti
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult, Principal principal, HttpServletResponse httpServletResponse) throws IOException {
        String email = principal.getName();
        logger.info("User " +  email + " creating user: " + user.getEmail());
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
        } else {
            userModel.saveUser(user);
            httpServletResponse.sendRedirect("/admin/courses");

        }
        return modelAndView;
    }
}

