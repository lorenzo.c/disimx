package com.disimx.disimx.model;

import com.disimx.disimx.entity.*;
import com.disimx.disimx.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by lorenzo on 5/28/17.
 */
@Service("courseService")
public class CourseModelImpl implements CourseModel {
    private static int YEAR_2018 = 2018;
    @Autowired
    private ScientificAreaRepository scientificAreaRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private CourseCourseRepository courseCourseRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private CoursePageRepository coursePageRepository;

    @Override
    public List<ScientificArea> getScientificAreas() {
        return scientificAreaRepository.findAll();
    }

    @Override
    public void save(Course course) {
        courseRepository.save(course);
    }

    @Override
    public Optional<Course> getCourse(Long courseId) {
        return courseRepository.findById(Long.valueOf(courseId));
    }

    @Override
    public boolean canModify(User prof, Long courseId) {
        return courseRepository.findByProfsAndId(prof, courseId) != null;
    }

    @Override
    public User getUser(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getProfs() {
        return userRepository.findByRoles(roleRepository.findByRole("Prof"));
    }

    @Override
    public List<Course> getCourses() {
        return courseRepository.findByYear(YEAR_2018);
    }

    @Override
    public Role getAdmin() {
        return roleRepository.findByRole("Admin");
    }

    @Override
    public Role getProf() {
        return roleRepository.findByRole("Prof");
    }

    @Override
    public List<Course> getCourses(User prof) {
        return courseRepository.findByProfs(prof);
    }

    @Override
    public Optional<Course> getCourse(String code, Integer year) {
        return courseRepository.findByCodeAndYear(code, year);
    }

    @Override
    public void saveRelation(Integer relation, Long relationCourseId, Long courseId) {
        CourseCourse courseRelation = new CourseCourse();
        courseRelation.setCourseId(courseId);
        courseRelation.setChildCourseId(relationCourseId);
        switch (relation) {
            case 1:
                courseRelation.setModule(1);
                break;
            case 2:
                courseRelation.setMutuato(1);
                break;
            case 3:
                courseRelation.setPropedeutico(1);
                break;
        }
        courseCourseRepository.save(courseRelation);

    }

    @Override
    public CourseCourse getRelationShip(Course course) {
        return courseCourseRepository.findFirstByCourse(course);
    }

    @Override
    public Page<Course> getCoursesPage(Pageable pageable) {
        return coursePageRepository.findAll(pageable);
    }

    @Override
    public Page<Course> getCoursesPageSearch(Pageable pageable, String keyword) {
        return coursePageRepository.findByNameContainingOrNameItContainingOrDescriptionContainingOrDescriptionItContaining(pageable, keyword, keyword, keyword, keyword);
    }

    @Override
    public List<Integer> getModuloRelationChildren(Course course) {
        if (course.getId() == null) return null;
        List<CourseCourse> relations = courseCourseRepository.findByCourseAndModule(course, 1);
        List<Integer> result = new ArrayList<>();
        if (relations != null)
            relations.forEach(r -> result.add(Math.toIntExact(r.getChildCourseId())));
        return result;
    }

    @Override
    public List<Integer> getMutuatoRelation(Course course) {
        if (course.getId() == null) return null;
        List<CourseCourse> relations = courseCourseRepository.findByCourseAndMutuato(course, 1);
        List<Integer> result = new ArrayList<>();
        if (relations != null)
            relations.forEach(r -> result.add(Math.toIntExact(r.getChildCourseId())));
        return result;
    }

    @Override
    public List<Integer> getPropedeuticoRelation(Course course) {
        if (course.getId() == null) return null;
        List<CourseCourse> relations = courseCourseRepository.findByCourseAndPropedeutico(course, 1);
        List<Integer> result = new ArrayList<>();
        if (relations != null)
            relations.forEach(r -> result.add(Math.toIntExact(r.getChildCourseId())));
        return result;
    }

    @Override
    public List<Course> getModuloRelationChildrenCourse(Course course) {
        if (course == null) return null;
        List<CourseCourse> relations = courseCourseRepository.findByCourseAndModule(course, 1);
        List<Course> result = new ArrayList<>();
        if (relations != null)
            relations.forEach(r -> result.add(courseRepository.findById(r.getChildCourseId()).get()));
        return result;
    }

    @Override
    public List<Course> getMutuatoRelationCourse(Course course) {
        if (course == null) return null;
        List<CourseCourse> relations = courseCourseRepository.findByCourseAndMutuato(course, 1);
        List<Course> result = new ArrayList<>();
        if (relations != null)
            relations.forEach(r -> result.add(courseRepository.findById(r.getChildCourseId()).get()));
        return result;
    }

    @Override
    public List<Course> getPropedeuticoRelationCourse(Course course) {
        if (course == null) return null;
        List<CourseCourse> relations = courseCourseRepository.findByCourseAndPropedeutico(course, 1);
        List<Course> result = new ArrayList<>();
        if (relations != null)
            relations.forEach(r -> result.add(courseRepository.findById(r.getChildCourseId()).get()));
        return result;
    }

    @Override
    public void saveRelations(Integer[] relationModuloId, Integer[] relationMutuatoId, Integer[] relationPropedeuticoId, Course course) {
        if (course != null && course.getId() != null)
            courseCourseRepository.deleteByCourseId(course.getId());
        if (relationModuloId != null) {
            for (Integer moduloCourseId : relationModuloId) {
                List<CourseCourse> relations = courseCourseRepository.findByCourseAndChildCourseIdAndModule(course, Long.valueOf(moduloCourseId), 1);
                if (relations == null || relations.size() == 0) {
                    CourseCourse relation = new CourseCourse();
                    relation.setCourseId(course.getId());
                    relation.setModule(1);
                    relation.setChildCourseId(Long.valueOf(moduloCourseId));
                    courseCourseRepository.save(relation);
                }
            }
        }
        if (relationMutuatoId != null) {
            for (Integer mutuatoCourseId : relationMutuatoId) {
                List<CourseCourse> relations = courseCourseRepository.findByCourseAndChildCourseIdAndMutuato(course, Long.valueOf(mutuatoCourseId), 1);
                if (relations == null || relations.size() == 0) {
                    CourseCourse relation = new CourseCourse();
                    relation.setCourseId(course.getId());
                    relation.setMutuato(1);
                    relation.setChildCourseId(Long.valueOf(mutuatoCourseId));
                    courseCourseRepository.save(relation);
                }
            }
        }
        if (relationPropedeuticoId != null) {
            for (Integer propedeuticoCourseId : relationPropedeuticoId) {
                List<CourseCourse> relations = courseCourseRepository.findByCourseAndChildCourseIdAndPropedeutico(course, Long.valueOf(propedeuticoCourseId), 1);
                if (relations == null || relations.size() == 0) {
                    CourseCourse relation = new CourseCourse();
                    relation.setCourseId(course.getId());
                    relation.setPropedeutico(1);
                    relation.setChildCourseId(Long.valueOf(propedeuticoCourseId));
                    courseCourseRepository.save(relation);
                }
            }
        }
    }

    @Override
    public List<Integer> getYears(Course course) {
        List<Course> courses = courseRepository.findByCode(course.getCode());
        List<Integer> result = new ArrayList<>();
        courses.forEach(t -> result.add(t.getYear()));
        return result;
    }

    @Override
    public Page<Course> getCoursesPage(Pageable pageable, User currentUser) {
        return coursePageRepository.findByProfs(pageable, currentUser);
    }

    @Override
    public ScientificArea getScientificArea(Integer scientificAreaId) {
        return scientificAreaRepository.findById(Long.valueOf(scientificAreaId)).get();
    }

    @Override
    public List<User> getProfs(Course course) {
        return courseRepository.findById(course.getId()).get().getProfs();
    }
}