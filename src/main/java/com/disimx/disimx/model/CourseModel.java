package com.disimx.disimx.model;


import com.disimx.disimx.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by lorenzo on 5/28/17.
 * Questa interfaccia stabilisce cosa sa fare un UserModel
 */
public interface CourseModel {
    List<ScientificArea> getScientificAreas();

    void save(Course course);

    Optional<Course> getCourse(Long courseId);

    boolean canModify(User prof, Long courseId);

    User getUser(String email);

    List<User> getProfs();

    List<Course> getCourses();

    Role getAdmin();

    Role getProf();

    List<Course> getCourses(User prof);

    Optional<Course> getCourse(String code, Integer year);

    void saveRelation(Integer relation, Long relationCourseId, Long courseId);

    CourseCourse getRelationShip(Course course);

    Page<Course> getCoursesPage(Pageable pageable);

    Page<Course> getCoursesPageSearch(Pageable pageable, String keyword);

    List<Integer> getModuloRelationChildren(Course course);

    List<Integer> getMutuatoRelation(Course course);

    List<Integer> getPropedeuticoRelation(Course course);

    List<Course> getModuloRelationChildrenCourse(Course course);

    List<Course> getMutuatoRelationCourse(Course course);

    List<Course> getPropedeuticoRelationCourse(Course course);

    void saveRelations(Integer[] relationModuloId, Integer[] relationMutuatoId, Integer[] relationPropedeuticoId, Course course);

    List<Integer> getYears(Course course);

    Page<Course> getCoursesPage(Pageable pageable, User currentUser);

    ScientificArea getScientificArea(Integer scientificAreaId);

    List<User> getProfs(Course course);
}
