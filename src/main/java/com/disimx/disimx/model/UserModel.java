package com.disimx.disimx.model;


import com.disimx.disimx.entity.User;

/**
 * Created by lorenzo on 5/28/17.
 * Questa interfaccia stabilisce cosa sa fare un UserModel
 */
public interface UserModel {
    void saveUser(User user);
}
