package com.disimx.disimx.model;

import com.disimx.disimx.entity.Role;
import com.disimx.disimx.entity.User;
import com.disimx.disimx.repository.RoleRepository;
import com.disimx.disimx.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by lorenzo on 5/28/17.
 */
@Service("userService")
public class UserModelImpl implements UserModel {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        Role role = null;
        if (user.getRoleId() != null) {
            role = roleRepository.findById(user.getRoleId());
        }
        Role userRole;
        if (role != null) {
            userRole = role;
        }
        else {
            userRole = roleRepository.findByRole("Prof");
        }
        user.setRoles(userRole);
        userRepository.save(user);
    }

}
